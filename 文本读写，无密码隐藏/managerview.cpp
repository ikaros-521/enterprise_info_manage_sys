#include "managerview.h"
#include "managerservice.h"

void Managerview::manager_sys(void)
{
	Managerservice ms;
	ms.load();
	if(!ms.login())
	{
		return;
	}
	while(1)
	{
		ms.menu();//通过控制台显示主菜单
		switch(get_cmd('0','3'))
		{
			case '1': ms.add(); break;
			case '2': ms.del(); break;
			case '3': ms.list(); break;
			case '0': ms.save(); return;
		}
	}
}
