#include "serviceview.h"
#include "manager.h"

void Serviceview::service_sys(void)
{
	Service s;
	s.load();
	if(!s.manager_login())
	{
		cout << "登陆失败" << endl;
		return;
	}
	while(1)
	{
		s.menu();
		switch(get_cmd('0','8'))
		{
			case '1': s.addDept(); break;
			case '2': s.deleteDept(); break;
			case '3': s.listDept(); break;
			case '4': s.addEmp(); break;
			case '5': s.deleteEmp(); break;
			case '6': s.modifyEmp(); break;
			case '7': s.listEmp(); break;
			case '8': s.listAllEmp(); break;
			case '0': s.save(); return;
		}
	}
}
