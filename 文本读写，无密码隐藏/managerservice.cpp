#include "managerservice.h"
#include <fstream>

#define MAX_M 10

Manager* m1[MAX_M];
int mid1 = 0,did1 = 0,eid1 = 0;

//超级管理员登陆
bool Managerservice::login(void)
{
	string a_name = "admin";
	string a_password = "admin";
	cout << "请输入用户名：";
	string name;
	cin >> name;
	cout << "请输入密码：";
	string password;
	cin >> password;
	if(name == a_name && password == a_password)
	{
		cout << "登陆成功" << endl;
		return true;
	}
	else
	{
		cout << "用户名或密码错误" << endl;
		return false;
	}
}

//通过控制台显示主菜单
void Managerservice::menu(void)
{
	cout << "****企业信息管理系统****" << endl;
	cout << "    1.增加管理员" << endl;
	cout << "    2.删除管理员" << endl;
	cout << "    3.列出所有管理员" << endl;
	cout << "    0.退出子系统" << endl;
	cout << "______________________" << endl;
}

//通过控制台处理增加管理员菜单项
void Managerservice::add(void)
{
	int m_num = 0;	// 现管理员数
	for(int i=0; i<MAX_M; i++)
	{
		if(m1[i]->get_id() == 0)
			continue;
		else
			m_num++;
	}
	if(m_num >= MAX_M)
	{
		cout << "管理员数已达上限，增加失败" << endl;
		return;
	}
	cout << "请输入管理员用户名：";
	string name;
	cin >> name;
	cout << "请输入管理员密码：";
	string password;
	cin >> password;
	
	for(int i=0; i<MAX_M; i++)
	{
		if(m1[i]->get_id() == 0)
		{
			m1[i]->set_id(mid1++);
			m1[i]->set_name(name);
			m1[i]->set_password(password);
			m1[i]->set_perm(0);
			break;		
		}
	}

	cout << "管理员添加成功" << endl;
}

//通过控制台处理删除管理员菜单项
void Managerservice::del(void)
{
	int m_num = 0;	// 现管理员数
	for(int i=0; i<MAX_M; i++)
	{
		if(m1[i]->get_id() == 0)
			continue;
		else
			m_num++;
	}
	if(m_num <= 0)
	{
		cout << "无管理员，删除失败" << endl;
		return;
	}
	cout << "请输入要删除的管理员编号：";
	int id = 0;
	cin >> id;
	for(int i=0; i<MAX_M; i++)
	{
		if(m1[i]->get_id() == id)
		{
			m1[i]->set_id(0);
			m1[i]->set_name("0");
			cout << "管理员删除成功" << endl;
			break;
		}
		if(i==MAX_M-1)
		{
			cout << "无此管理员，删除失败" << endl;
			return;
		}
	}
}

//通过控制台处理列出所有管理员菜单项
void Managerservice::list(void)
{
	for(int i=0; i<MAX_M; i++)
	{
		if(m1[i]->get_id() != 0)
		{
			cout << "管理员id：" << m1[i]->get_id();
			cout << " ,管理员用户名：" << m1[i]->get_name();
			//cout << " ,管理员密码：" << d[i]->get_password();
			cout << "，管理员锁：" << m1[i]->get_perm() << endl;
		}	
	}
}

//从数据存储读取管理员信息
void Managerservice::load(void)
{
	fstream r2("data/id.txt",ios::in);
	if(!r2.good())
	{
		cout << "id.txt数据加载异常" << endl;
	}
	r2 >> mid1 >> did1 >> eid1;

	fstream r4("data/managers.txt",ios::in);
	if(!r4.good())
	{
		cout << "managers.txt数据加载异常" << endl;
	}

	for(int i=0; i<MAX_M; i++)
	{
		int id;
		string name;
		string password;
		long perm;
		r4 >> id >> name;
		r4 >> password >> perm;
		m1[i] = new Manager(id,name,password,perm);		
	}
}

//将管理员信息写入数据存储
void Managerservice::save(void)
{
	fstream r2("data/id.txt",ios::out);
	r2 << mid1 << " " << did1 << " " << eid1;


	fstream r4("data/managers.txt",ios::out);
	for(int i=0; i<MAX_M; i++)
	{
		int id = m1[i]->get_id();
		string name = m1[i]->get_name();
		string password = m1[i]->get_password();
		long perm = m1[i]->get_perm();
		r4 << id <<" "<< name << " ";
		r4 << password <<" "<< perm <<"\n";		
	}
}
