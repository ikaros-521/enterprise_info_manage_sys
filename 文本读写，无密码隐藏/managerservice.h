#ifndef MANAGERSERVICE_H
#define MANAGERSERVICE_H

#include <iostream>
#include <string.h>
#include "manager.h"

using namespace std;

class Managerservice
{
public:
	bool login(void);//超级管理员登陆
	void menu(void);//通过控制台显示主菜单
	void add(void);//通过控制台处理增加管理员菜单项
	void del(void);//通过控制台处理删除管理员菜单项
	void list(void);//通过控制台处理列出所有管理员菜单项
	void load(void);//从数据存储读取管理员信息
	void save(void);//将管理员信息写入数据存储

};

#endif//MANAGERSERVICE_H
