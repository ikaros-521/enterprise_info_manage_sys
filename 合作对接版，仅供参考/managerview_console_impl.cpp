#include<stdlib.h>
#include"emis.h"
#include<getch.h>
#include<fstream>
#include<list>
#include<string>
#include"manager.h"
#include"managerservice_impl.h"
#include"managerdao_file_impl.h"
#include"managerview_console_impl.h"
#include"tools.h"

//显示主菜单
void Managerview_console_impl::menu(void)
{
	while(1)
	{
		system("clear");	
		cout<<"----超级管理员操作界面----"<<endl;
		cout<<"    1.增加管理员"<<endl;
		cout<<"    2.删除管理员"<<endl;
		cout<<"    3.列出所有管理员"<<endl;
		cout<<"    4.退出"<<endl;
		switch(getch())
		{
			case'1':mci.add();break;
			case'2':mci.del();break;
			case'3':mci.list();break;
			case'4':system("clear");
					cout<<">退出...任意键继续..."<<endl;
					stdin->_IO_read_ptr =stdin->_IO_read_end;
					getch();
					return;
		}
	}	
}

//处理增加管理员菜单项
void Managerview_console_impl::add(void)
{

	system("clear");	
	cout<<"----处理增加管理员菜单项----"<<endl;
	cout<<"请输入管理员姓名："<<endl;
	string common_admin_name = "0";
	cin>>common_admin_name;
	cout<<"请输入管理员密码(6位)："<<endl;
	string common_admin_password ="0";	
	cin>>common_admin_password;
	Manager add(0,common_admin_name,common_admin_password,0);
	
	Managerdao_file_impl dao;
	dao.save(add.get_id(),add.get_name(),add.get_password());
	cout<<"添加成功！...任意键继续..."<<endl;
	stdin->_IO_read_ptr =stdin->_IO_read_end;
	getch();
	return;

}



//处理删除管理员菜单项
void Managerview_console_impl::del(void)
{
	system("clear");
	cout<<"-----删除管理员菜单项-----"<<endl;
//	mci.list();

	FILE *fin=fopen("data/manager.txt","r");	
	char ch;
	int n=1;
	cout<<n<<".";
	while((ch=fgetc(fin))!=EOF)
	{	
		cout<<ch;
		if(ch =='\n')
		{
			n++;
			cout<<n<<".";
		}	
	}
	fclose(fin);

	cout<<endl<<"请输入删除的序号:";
	int lineNum =0;
	cin>>lineNum;
	Delete("data/manager.txt",lineNum);
	cout<<endl<<"删除成功"<<endl;
	cout<<"-----管理员菜单项-----"<<endl;
	Managerdao_file_impl dao;
	dao.load();
	cout<<endl<<endl<<"...任意键继续..."<<endl;
	stdin->_IO_read_ptr =stdin->_IO_read_end;
	getch();
}

//处理列出所有管理员菜单项
void Managerview_console_impl::list(void)
{
	system("clear");
	cout<<"-----管理员菜单项-----"<<endl;
	Managerdao_file_impl dao;
	dao.load();
	cout<<endl<<endl<<"...任意键继续..."<<endl;
	stdin->_IO_read_ptr =stdin->_IO_read_end;
	getch();
}


