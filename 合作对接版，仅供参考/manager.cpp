#include "manager.h"
#include <string.h>
#include <algorithm>
#include <vector>
#include <stdlib.h>
#include <time.h>
#include "emis.h"


Manager::Manager(int _id,string _name,string _password,long _perm)
{
	srand(time(NULL));
	id = 1000000 + rand()%100000;
	name=_name;
	password=_password;
	perm = 0;
}

string Manager::get_name(void)
{
	return name;
}

string Manager::get_password(void)
{
	return password;
}

int Manager::get_id(void)
{
	return id;
}

long Manager::get_perm(void)
{
	return perm;
}

void Manager::set_perm(long cperm)
{
	cperm = perm;
}



