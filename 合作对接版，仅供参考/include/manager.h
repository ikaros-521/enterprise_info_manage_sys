#ifndef MANAGER_H
#define MANAGER_H
#include<iostream>

using namespace std;


class Manager
{
	int id;				//id
	string name;		//用户名
	string password;	//密码
	long perm;			//锁
	
public:
	Manager(int _id,string _name,string _password,long _perm);
	
	string get_name(void);
	string get_password(void);
	int get_id(void);
	long get_perm(void);
	void set_perm(long cperm);
};

#endif//MANAGER_H
