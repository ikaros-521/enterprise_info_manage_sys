#ifndef MANAGERSERVICE_H
#define MANAGERSERVICE_H

#include<iostream>

using namespace std;

class Managerservice
{
public:
	virtual void addManager(Manager manager) = 0;		//增加管理员
	virtual void deleteManager(void) = 0;	//删除管理员
	virtual void listManager(void) = 0;		//列出所有管理员
};

#endif//MANAGERSERVICE_H
