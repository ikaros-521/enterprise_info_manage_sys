#ifndef MANAGERDAO_FILE_IMPL_H
#define MANAGERDAO_FILE_IMPL_H

#include"managerdao.h"



class Managerdao_file_impl:public Managerdao
{

public:
	void load(void);//从文件读取管理员信息。以二进制方式整块读取Manager对象，加入从参数传入的管理员容器
	void save(int,string,string);//将管理员信息写入文件。遍历从参数传入的管理员容器，以二进制方式整块写入每一个Manager对象


};

#endif//MANAGERDAO_FILE_IMPL_H
