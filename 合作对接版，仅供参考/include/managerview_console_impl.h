#ifndef MANAGERVIEW_CONSOLE_IMPL_H
#define MANAGERVIEW_CONSOLE_IMPL_H

#include "managerview.h"



class Managerview_console_impl:public Managerview
{
public:
	void menu(void);	//显示主菜单
	void add(void);		//处理增加管理员菜单项
	void del(void);		//处理删除管理员菜单项
	void list(void);	//处理列出所有管理员菜单项

};

#endif//MANAGERVIEW_CONSOLE_IMPL_H
