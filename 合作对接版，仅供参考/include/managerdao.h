#ifndef MANAGERDAO_H
#define MANAGERDAO_H

#include<iostream>

using namespace std;

class Managerdao
{
public:
	virtual void load(void)=0;//从数据存储读取管理员信息
	virtual void save(int,string,string)=0;//将管理员信息写入数据存储

};

#endif//MANAGERDAO_H
