#ifndef TOOLS_H
#define TOOLS_H
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <fstream>
#include <ostream>
#include <cstdlib>
#include <string.h>
#include "getch.h"
#include <stdbool.h>
 
using namespace std;
extern void Admin_login_check(void);
extern void Delete(const char* filepath, int n);

void clear_stdin(void);

char get_cmd(char start,char end);

char* get_pw(char* passwd,bool is_show,size_t size);

#endif//TOOLS_H
