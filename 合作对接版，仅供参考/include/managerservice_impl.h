#ifndef MANAGERSERVICE_IMPL_H
#define MANAGERSERVICE_IMPL_H
#include<vector>
#include "manager.h"
#include "managerservice.h"
#include "managerdao.h"
class Managerservice_impl:public Managerservice
{
public:

	Managerdao* dao;			//数据访问对象。
	void addManager(Manager manager);//增加管理员
	void deleteManager(void);	//删除管理员
	void listManager(void);		//列出所有管理员
	
};

#endif//MANAGERSERVICE_IMPL_H
